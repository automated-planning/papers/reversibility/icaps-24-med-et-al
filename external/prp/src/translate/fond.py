from collections import defaultdict
import itertools

FOND_FILE_VERSION = 1


class FONDTask:
    def __init__(self, variables, mutexes, init, goal, operators, axioms, metric):
        self.variables = variables
        self.mutexes = mutexes
        self.init = init
        self.goal = goal
        self.operators = sorted(operators, key=lambda op: (op.name, op.precondition))
        self.axioms = sorted(axioms, key=lambda axiom: (axiom.condition, axiom.effect))
        self.metric = metric

    def output(self, stream):
        print("begin_version", file=stream)
        print(FOND_FILE_VERSION, file=stream)
        print("end_version", file=stream)
        print("begin_metric", file=stream)
        print(int(self.metric), file=stream)
        print("end_metric", file=stream)
        self.variables.output(stream)
        # print(len(self.mutexes), file=stream)
        # for mutex in self.mutexes:
        #     mutex.output(stream)
        # self.init.output(stream)
        # self.goal.output(stream)
        print(len(self.operators), file=stream)
        for op in self.operators:
            op.output(stream)
        assert len(self.axioms) == 0
        # print(len(self.axioms), file=stream)
        # for axiom in self.axioms:
        #     axiom.output(stream)

    def get_encoding_size(self):
        task_size = 0
        task_size += self.variables.get_encoding_size()
        for mutex in self.mutexes:
            task_size += mutex.get_encoding_size()
        task_size += self.goal.get_encoding_size()
        for op in self.operators:
            task_size += op.get_encoding_size()
        for axiom in self.axioms:
            task_size += axiom.get_encoding_size()
        return task_size


class FONDOperator:
    def __init__(self, name, precondition, effects, cost):
        self.name = name
        self.precondition = sorted(precondition)
        self.effects = sorted(effects)
        self.cost = cost

    def output(self, stream):
        print("begin_action", file=stream)
        print(self.name[1:-1], file=stream)
        print(len(self.precondition), file=stream)
        for var, val in self.precondition:
            print(var, val, file=stream)
        print(len(self.effects), file=stream)
        for effect in self.effects:
            print(len(effect), file=stream)
            for var, val, cond in effect:
                assert len(cond) == 0
                print(var, val, file=stream)
        print(self.cost, file=stream)
        print("end_action", file=stream)

    def get_encoding_size(self):
        size = 1 + len(self.precondition)
        for effect in self.effects:
            for var, post, cond in effect:
                size += 1 + len(cond)
        return size


def group_actions(actions):
    stochastized_actions = defaultdict(list)
    for action in actions:
        head, sep, tail = action.name.partition("-DETDUP-")
        common_name = head + tail.lstrip("0123456789")
        stochastized_actions[common_name].append(action)
    return stochastized_actions


def stochastize_actions(all_actions):
    def get_preconditions(action):
        return set(action.prevail + [(var, pre) for var, pre, _, _ in action.pre_post if pre != -1])

    def get_effects(action):
        return list((var, post, cond) for var, _, post, cond in action.pre_post)

    stochastized_actions = []
    grouped_actions = group_actions(all_actions)
    for action_name, actions in grouped_actions.items():
        precondition = get_preconditions(actions[0])
        assert all(precondition == get_preconditions(other_action) for other_action in actions[1:])
        effects = list(map(get_effects, actions))
        cost = actions[0].cost
        assert all(other_action.cost == cost for other_action in actions[1:])
        stochastized_actions.append(FONDOperator(action_name, precondition, effects, cost))
    return stochastized_actions


def sas_to_fond(sas_task):
    return FONDTask(
        variables=sas_task.variables,
        mutexes=sas_task.mutexes,
        init=sas_task.init,
        goal=sas_task.goal,
        operators=stochastize_actions(sas_task.operators),
        axioms=sas_task.axioms,
        metric=sas_task.metric,
    )

# Weak and Strong Reversibility of Non-Deterministic Actions: Universality and Uniformity

A repository containing experiments of the paper called Weak and Strong Reversibility of Non-Deterministic Actions: Universality and Uniformity published at ICAPS 2024.

https://icaps24.icaps-conference.org/

## Requirements

1) `cmake`
2) `make`
3) `g++`
4) `python3` (supported version: `3.11`)
5) `bash`
6) Linux OS recommended (Windows OS has not been tested and we expect him to fail)

## Instalation

1) Install both PRP and FD planners.
   1) Planner for Relevant Policies: https://github.com/QuMuLab/planner-for-relevant-policies
   2) Fast Downward: https://www.fast-downward.org/

2) Set the paths to Fast Downward and PRP planner in `codes/scripts/paths.py` script. (We recommend absolute paths.)

## Running Experiments

Use the script `codes/scripts/run_experiment.py <DOMAIN>` to run experiment on domain `<DOMAIN>`. The list of available experiments can be view in the beginning of the script. We recomment to run in inside `codes/scripts` folder.

The output can be found in the folder `<WORKING_DIRECTORY>/results/<DOMAIN>/` as well as in the script's `stdout`.

## How to Cite

### Plain Text

Jakub Med, Lukas Chrpa, Michael Morak, Wolfgang Faber (2024). Weak and Strong Reversibility of Non-Deterministic Actions: Universality and Uniformity. In 34th International Conference on Automated Planning and Scheduling.

### BibTeX

    @inproceedings{
        med2024weak,
        title={Weak and Strong Reversibility of Non-Deterministic Actions: Universality and Uniformity},
        author={Jakub Med and Lukas Chrpa and Michael Morak and Wolfgang Faber},
        booktitle={34th International Conference on Automated Planning and Scheduling},
        year={2024},
        url={https://openreview.net/forum?id=ABcIhcbIqH}
    }

## Contact

In a case of any problems or questions, please contact the authors.

* Jakub Med; jakub.med@cvut.cz; CIIRC & FEE, CTU
* Lukáš Chrpa; chrpaluk@cvut.cz; CIIRC, CTU
* Michael Morak; Michael.Morak@aau.at; AAU Klagenfurt
* Wolfgang Faber; Wolfgang.Faber@aau.at; AAU Klagenfurt


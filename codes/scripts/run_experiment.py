#!/usr/bin/env python3

import itertools
import time
from typing import Literal

import os
import sys
import argparse
import subprocess
import collections
import yaml

import paths


parser = argparse.ArgumentParser()
parser.add_argument("benchmark", type=str)
args = parser.parse_args()

prp_experiments = {
    "blocksworld": ("../../domains/prp/blocksworld/domain.pddl", "../../domains/prp/blocksworld/p1.pddl"),
    "ex-blocksworld": ("../../domains/prp/ex-blocksworld/domain.pddl", "../../domains/prp/ex-blocksworld/p01.pddl"),
    "elevators": ("../../domains/prp/elevators/domain.pddl", "../../domains/prp/elevators/p01.pddl"),
    "forest": ("../../domains/prp/forest/domain.pddl", "../../domains/prp/forest/p_3_1.pddl"),
    "tireworld": ("../../domains/prp/tireworld/domain.pddl", "../../domains/prp/tireworld/p01.pddl"),
    "zenotravel": ("../../domains/prp/zenotravel/domain.pddl", "../../domains/prp/zenotravel/p01.pddl"),
    "first-responders": (
        "../../domains/prp/first-responders/domain.pddl",
        "../../domains/prp/first-responders/p_3_3.pddl",
    ),
    "bus-fare": (
        "../../domains/prp/interesting/bus-fare-domain.pddl",
        "../../domains/prp/interesting/bus-fare-prob.pddl",
    ),
    "climber": ("../../domains/prp/interesting/climber-domain.pddl", "../../domains/prp/interesting/climber-prob.pddl"),
    "river": ("../../domains/prp/interesting/river-domain.pddl", "../../domains/prp/interesting/river-prob.pddl"),
    "faults": ("../../domains/prp/faults/d_5_5.pddl", "../../domains/prp/faults/p_5_5.pddl"),
}

fond_sat_experiments = {
    "acrobatics": ("../../domains/fond-sat/acrobatics/domain.pddl", "../../domains/fond-sat/acrobatics/p01.pddl"),
    "beam-walk": ("../../domains/fond-sat/beam-walk/domain.pddl", "../../domains/fond-sat/beam-walk/p01.pddl"),
    "doors": ("../../domains/fond-sat/doors/domain.pddl", "../../domains/fond-sat/doors/p01.pddl"),
    "earth-observation": ("../../domains/fond-sat/earth-observation/domain.pddl", "../../domains/fond-sat/earth-observation/p01.pddl"),
    "islands": ("../../domains/fond-sat/islands/domain.pddl", "../../domains/fond-sat/islands/p01.pddl"),
    "miner": ("../../domains/fond-sat/miner/domain.pddl", "../../domains/fond-sat/miner/p01.pddl"),
    "spiky-tireworld": ("../../domains/fond-sat/spiky-tireworld/domain.pddl", "../../domains/fond-sat/spiky-tireworld/p01.pddl"),
    "tireworld-truck": ("../../domains/fond-sat/tireworld-truck/domain.pddl", "../../domains/fond-sat/tireworld-truck/p01.pddl"),
    "triangle-tireworld": ("../../domains/fond-sat/triangle-tireworld/domain.pddl", "../../domains/fond-sat/triangle-tireworld/p01.pddl"),
}

experiments = prp_experiments | fond_sat_experiments


def run_translate(domain, problem, args, compilation: Literal["group", "weak", "strong", "irreversibility"]):
    start_time = time.time_ns()
    subprocess.run(
        [
            "python3",
            "../../external/prp/src/translate/translate.py",
            "300",
            domain,
            problem,
            f"./compilations/{args.benchmark}",
            compilation,
            "-a",
            "<all_actions>",
        ],
        stdin=subprocess.DEVNULL,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.STDOUT,
    )
    end_time = time.time_ns()
    print(f"translation time for {args.benchmark} and compilation={compilation}: {((end_time - start_time) / (10**6)):.2f} ms")


def run_downward(problem):
    result = subprocess.run(
        [
            os.path.join(paths.DOWNWARD_PATH, "fast-downward.py"),
            "--search",
            "--alias",
            "lama-first",
            problem,
        ],
        stdin=subprocess.DEVNULL,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.STDOUT,
    )
    return result


def run_prp(problem):
    result = subprocess.run(
        [
            "/bin/bash",
            "../../external/prp/src/run-prp-no-translate",
            os.path.join(paths.PRP_PATH, "src/"),
            problem,
            "--dump-policy",
            "2",
            "--optimize-final-policy",
            "1"
        ],
        capture_output=True,
    )
    if os.path.isfile("./policy.out"):
        policy = load_policy("./policy.out")
        os.remove("./policy.out")
    else:
        policy = None
    if os.path.isfile("./policy.fsap"):
        os.remove("./policy.fsap")
    return result, policy

def load_policy(path):
    policy = {}
    with open(path, "r") as f:
        lines = f.readlines()
        load_subset = True
        for line in lines:
            stripped_line = line.strip() 
            if stripped_line != "":
                if load_subset:
                    assert stripped_line.startswith("If holds: ")
                    string_facts = stripped_line.removeprefix("If holds: ").strip().split()
                    substate = frozenset(map(lambda x: tuple(map(str, x.split(":", 1))), string_facts))
                    load_subset = False
                else:
                    assert stripped_line.startswith("Execute: ")
                    action = stripped_line[9:stripped_line.find("/")].strip()
                    if action != "goal":
                        policy[substate] = action
                    load_subset = True
    return policy

def run_weak_phi_rev(problem, action_name):
    result = subprocess.run(
        [
            "../build/reverse_one",
            problem,
            action_name,
        ],
        capture_output=True,
    )
    return result


def decide_UU_weak(folder):
    total_time = 0
    i = 0
    successes = 0
    for problem_file in os.scandir(folder):
        i += 1
        start_time = time.time_ns()
        result = run_downward(problem_file)
        end_time = time.time_ns()
        total_time += end_time - start_time
        if result.returncode == 0:
            successes += 1
    return successes == i, total_time


def decide_UU_strong(folder):
    total_time = 0
    i = 0
    successes = 0
    policies = []
    for problem_file in os.scandir(folder):
        i += 1
        start_time = time.time_ns()
        result, policy = run_prp(problem_file)
        end_time = time.time_ns()
        total_time = end_time - start_time
        if len(result.stderr.decode("utf-8")) > 0:
            print(result.stderr.decode("utf-8"), file=sys.stderr)
        decoded_stdout = str(result.stdout)
        planning_passed = "plan found." in decoded_stdout
        if planning_passed:
            successes += 1
        policies.append(policy)
    if successes == i:
        if len(list(os.scandir(folder))) == 1:
            return True, "", total_time, policies
        else:
            for p1, p2 in itertools.combinations(policies, 2): 
                for (subset_p1, _), (subset_p2, _) in itertools.product(p1.items(), p2.items()):
                    if not any(v1 == v2 and x != y for v1, x in subset_p1 for v2, y in subset_p2):
                        return False, "merge", total_time, policies
            return True, "", total_time, policies
    else:
        return False, "plan", total_time, None


def decide_UU_irreversibility(folder):
    total_time = 0
    i = 0
    successes = 0
    for problem_file in os.scandir(folder):
        i += 1
        start_time = time.time_ns()
        result = run_downward(problem_file)
        end_time = time.time_ns()
        total_time += end_time - start_time
        if result.returncode == 0:
            successes += 1
    return successes != i, total_time


def decide_weak_varphi(domain_path, action_name):
    revert_result = run_weak_phi_rev(domain_path, action_name.replace("_", " "))
    if len(revert_result.stderr.decode("utf-8")) > 0:
        print(revert_result.stderr.decode("utf-8"), file=sys.stderr)
    return revert_result.stdout.decode("utf-8").strip()


def main():
    domain, problem = experiments[args.benchmark]

    run_translate(domain, problem, args, "weak")
    run_translate(domain, problem, args, "strong")
    run_translate(domain, problem, args, "group")
    run_translate(domain, problem, args, "irreversibility")

    reversibility_result = {}
    time_result = collections.defaultdict(dict)
    for f in os.scandir(f"./compilations/{args.benchmark}/weak"):
        if f.is_dir:
            print(f"Determining reversibility of {f.name}...", end=" ", flush=True)
            if os.path.isfile(os.path.join(f, "violated.info")):
                print(f"violated", end="", flush=True)
                irreversible, time = decide_UU_irreversibility(
                    os.path.join(f"./compilations/{args.benchmark}/irreversibility", f.name)
                )
                time_result["irreversibility"][f.name] = time
                if irreversible:
                    weak_varphi_result = "irreversible"
                else:
                    weak_varphi_result = "?"
                print(f"-{weak_varphi_result}")
                reversibility_result[f.name] = f"violated-{weak_varphi_result}"
            else:
                weak_reverted, time = decide_UU_weak(f)
                time_result["weak"][f.name] = time
                if weak_reverted:
                    print(f"weak", end="", flush=True)
                    strong_reverted, reason, time, policies = decide_UU_strong(
                        os.path.join(f"./compilations/{args.benchmark}/strong", f.name)
                    )
                    time_result["strong"][f.name] = time
                    reversibility_result[f.name] = "strong" if strong_reverted else f"weak-failed-{reason}"
                    if strong_reverted:
                        print(f", strong")
                    else:
                        print(f", failed-{reason}")
                else:
                    print(f"failed", end="", flush=True)
                    irreversible, time = decide_UU_irreversibility(
                        os.path.join(f"./compilations/{args.benchmark}/irreversibility", f.name)
                    )
                    time_result["irreversibility"][f.name] = time
                    if irreversible:
                        weak_varphi_result = "irreversible"
                    else:
                        weak_varphi_result = "?"
                    print(f"-{weak_varphi_result}")
                    reversibility_result[f.name] = f"failed-{weak_varphi_result}"

    results = collections.Counter(reversibility_result.values())

    os.makedirs(os.path.join("results", args.benchmark), exist_ok=True)
    with open(os.path.join("results", args.benchmark, "reversibility_result.yaml"), "w") as f:
        yaml.dump(reversibility_result, f)
    with open(os.path.join("results", args.benchmark, "time_results.yaml"), "w") as f:
        yaml.dump(dict(time_result), f)

    print(f"Reversibility of {args.benchmark} decided:")
    for key, count in results.items():
        print(f"\t{key}: {count}")


if __name__ == "__main__":
    main()
